/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG;

import sysRPG.domains.*;

/**
 *
 * @author Luke Frozz
 * @since 20/09/2015
 */
public class SysRPG {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Personagem p = new Personagem();
        Tendencia tp = new Tendencia();
        p.setNome("Lucius");
        p.setIdade(19);
        p.setSexo(Sexo.Masculino);
        p.setTamanho(Tamanho.M);
        tp.setNomePT("Benfeitor");
        tp.setNomeING("Neutral Good");
        tp.setMoral(1);
        tp.setEtica(2);
        p.setTendencia(tp);
        Raca r = new Raca();
        Atributos ar = new Atributos();
        Tendencia tr = new Tendencia();
        r.setNome("Elfo");
        ar.setForca(0);
        ar.setDestreza(4);
        ar.setConstituicao(-2);
        ar.setInteligencia(2);
        ar.setSabedoria(0);
        ar.setCarisma(0);
        r.setAlteracaoAtributos(ar);
        tr.setMoral(3);
        tr.setEtica(1);
        tr.setNomePT("Rebelde");
        tr.setNomeING("Chaotic Good");
        r.setTendenciaPadrao(tr);
        p.setRaca(r);
        p.setTendencia(r.getTendenciaPadrao());
        Atributos ap = new Atributos();
        ap.setForca(2);
        ap.setDestreza(7);
        ap.setConstituicao(8);
        ap.setInteligencia(11);
        ap.setSabedoria(15);
        ap.setCarisma(19);
        p.setAtributos(ap);
        System.out.println(Views.PersonagemPure(p));
    }
    
}
