package sysRPG.domains;

/**
 * Enumerador de Tamanho de uma Personagem
 * @author Luke Frozz
 * @since 25/09/2015
 */
public enum Tamanho {
	/**
	 * Pequeno
	 */
	P,
	/**
	 * M�dio
	 */
	M,
	/**
	 * Grande
	 */
	G,
	/**
	 * Extra Grande
	 */
	GG,
	/**
	 * Enorme
	 */
	E,
	/**
	 * Colossal
	 */
	C
}
