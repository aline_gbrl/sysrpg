/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe de visualiza��o dos Personagens
 * seus atributos e as altera��es consecutivas
 * @author Luke Frozz
 * @since 24/09/2015
 */
public class Views {
    /**
     * Visualiza��o do Personagem com atributos
     * comum, sem nenhuma modifica��o de ra�a ou atributo
     * @param p Personagem a ser visualizado
     */
    public static String PersonagemPure(Personagem p) {
        StringBuilder sb = new StringBuilder();
        sb.append("|--------------------------------|\n");
        sb.append(String.format("|Personagem: %20s|\n", p.getNome()));
        sb.append("|--------------------------------|\n");
        sb.append(String.format("|Idade: %25s|\n", (
            p.getIdade() == null ? "desconhecida" : p.getIdade())));
        sb.append(String.format("|Sexo: %10s|Tamanho: %6s|\n", 
    		p.getSexo(), p.getTamanho()));
        sb.append(String.format("|Tend�ncia: %21s|\n", 
            String.format("%s e %s", p.getTendencia().getMoral(),
                p.getTendencia().getEtica())));
        sb.append(String.format("|%32s|\n", 
            String.format("%s / %s",
                p.getTendencia().getNomePT(),
                p.getTendencia().getNomeING())));
        sb.append(String.format("|Ra�a: %26s|\n", p.getRaca().getNome()));
        sb.append("|------------ATRIBUTOS-----------|\n");
        sb.append("|-NOMEC-|--VLR--|--RA�A--|--MOD--|\n");
        sb.append(String.format("|  FOR%8s%9s%8s%3s\n", 
    		p.getAtributos().getForca(),
    		p.getRaca().getAlteracaoAtributos().getForca(),
    		Atributos.mod(p.getAtributos().getForca() + 
				p.getRaca().getAlteracaoAtributos().getForca()),
    		"|"));
        sb.append(String.format("|  DES%8s%9s%8s%3s\n", 
    		p.getAtributos().getDestreza(),
    		p.getRaca().getAlteracaoAtributos().getDestreza(),
    		Atributos.mod(p.getAtributos().getDestreza() + 
				p.getRaca().getAlteracaoAtributos().getDestreza()),
    		"|"));
        sb.append(String.format("|  CON%8s%9s%8s%3s\n", 
    		p.getAtributos().getConstituicao(),
    		p.getRaca().getAlteracaoAtributos().getConstituicao(),
    		Atributos.mod(p.getAtributos().getConstituicao() + 
				p.getRaca().getAlteracaoAtributos().getConstituicao()),
    		"|"));
        sb.append(String.format("|  INT%8s%9s%8s%3s\n", 
    		p.getAtributos().getInteligencia(),
    		p.getRaca().getAlteracaoAtributos().getInteligencia(),
    		Atributos.mod(p.getAtributos().getInteligencia() + 
				p.getRaca().getAlteracaoAtributos().getInteligencia()),
    		"|"));
        sb.append(String.format("|  SAB%8s%9s%8s%3s\n", 
    		p.getAtributos().getSabedoria(),
    		p.getRaca().getAlteracaoAtributos().getSabedoria(),
    		Atributos.mod(p.getAtributos().getSabedoria() + 
				p.getRaca().getAlteracaoAtributos().getSabedoria()),
    		"|"));
        sb.append(String.format("|  CAR%8s%9s%8s%3s\n", 
    		p.getAtributos().getCarisma(),
    		p.getRaca().getAlteracaoAtributos().getCarisma(),
    		Atributos.mod(p.getAtributos().getCarisma() + 
				p.getRaca().getAlteracaoAtributos().getCarisma()),
    		"|"));
        return sb.toString();
    }
}
