/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de um Slot de perícia
 * Onde contém a perícia tida pelo personagem junto
 * a sua especialização da mesma
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class SlotPericia {
	/**
	 * Per�cia do Slot
	 */
    private Pericia pericia;
    /**
     * Pontos da Per�cia
     */
    private Integer pontos;
    
    /**
     * Getter de Per�cia
     * @return Per�cia
     */
    public Pericia getPericia() {
        return pericia;
    }
    /**
     * Setter dePer�cia
     * @param pericia atribui a Per�cia do Slot
     */
    public void setPericia(Pericia pericia) {
        this.pericia = pericia;
    }
    /**
     * Getter de Pontos
     * @return Pontos da Per�cia
     */
    public Integer getPontos() {
        return pontos;
    }
    /**
     * Setter de Pontos
     * @param pontos atribui os Pontos da Per�cia
     */
    public void setPontos(Integer pontos) {
        this.pontos = pontos;
    }
    
    /**
     * Construtor padr�o de Per�cia
     */
    public SlotPericia(){
    	
    }
    
}
