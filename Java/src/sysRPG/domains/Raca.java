/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representativa de Ra�a
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Raca {
	/**
	 * Id da Ra�a
	 */
    private Long id;
    /**
     * Nome da Ra�a
     */
    private String Nome;
    /**
     * Descri��o da Ra�a
     */
    private String Descricao;
    /**
     * Tend�ncia padr�o da Ra�a
     */
    private Tendencia tendenciaPadrao;
    /**
     * Tamanho Padr�o da Ra�a
     */
    private Tamanho tamanhoPadrao;
    /**
     * Alteradores de Atributos da Ra�a
     */
    private Atributos alteracaoAtributos;
    /**
     * Alteradores de Per�cia da Ra�a
     */
    private List<SlotPericia> alteracaoPericias;

    /**
     * Getter de Id
     * @return Id da Ra�a
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id da Ra�a
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome da Ra�a
     */
    public String getNome() {
        return Nome;
    }
    /**
     * Setter de Nome
     * @param Nome atribui o Nome da Ra�a
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    /**
     * Getter de Descri��o
     * @return Descri��o da Ra�a
     */
    public String getDescricao() {
        return Descricao;
    }
    /**
     * Setter de Descri��o
     * @param Descricao atribui a Descri��o da Ra�a
     */
    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }
    /**
     * Getter de tendenciaPadrao
	 * @return Tend�ncia padr�o da Ra�a
	 */
	public Tendencia getTendenciaPadrao() {
		return tendenciaPadrao;
	}
	/**
	 * Setter de tendenciaPadrao
	 * @param tendenciaPadrao atribui a Tend�ncia padr�o da Ra�a
	 */
	public void setTendenciaPadrao(Tendencia tendenciaPadrao) {
		this.tendenciaPadrao = tendenciaPadrao;
	}
	/**
	 * Getter de tamanhoPadrao
	 * @return Tamanho padr�o da Ra�a
	 */
	public Tamanho getTamanhoPadrao() {
		return tamanhoPadrao;
	}
	/**
	 * Setter de tamanhoPadrao
	 * @param tamanhoPadrao atribui o Tamanho padr�o da Ra�a
	 */
	public void setTamanhoPadrao(Tamanho tamanhoPadrao) {
		this.tamanhoPadrao = tamanhoPadrao;
	}
	/**
     * Getter de alteracaoAtributos
     * @return Alteradores de Atributos da Ra�a
     */
    public Atributos getAlteracaoAtributos() {
        return alteracaoAtributos;
    }
    /**
     * Setter de alteracaoAtributos
     * @param alteracaoAtributos atribui os alteradores de Atributos da Ra�a
     */
    public void setAlteracaoAtributos(Atributos alteracaoAtributos) {
        this.alteracaoAtributos = alteracaoAtributos;
    }
    /**
     * Getter de alteracaoPericias
     * @return Alteradores de Per�cia da Ra�a
     */
    public List<SlotPericia> getAlteracaoPericias() {
        return alteracaoPericias;
    }
    /**
     * Setter de alteracaoPericias
     * @param alteracaoPericias atribui os alteradores de Per�cia da Ra�a
     */
    public void setAlteracaoPericias(List<SlotPericia> alteracaoPericias) {
        this.alteracaoPericias = alteracaoPericias;
    }
    
    /**
     * Coonstrutor Padr�o de Ra�a
     */
    public Raca(){
    	tendenciaPadrao = new Tendencia();
    	alteracaoAtributos = new Atributos();
    	alteracaoPericias = new ArrayList<>();
    }
}
