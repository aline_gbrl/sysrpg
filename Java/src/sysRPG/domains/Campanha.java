/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

import java.time.LocalDate;
import java.util.*;

/**
 * Classe representativa da Campanha
 * @author Luke Frozz
 * @since 23/09/2015
 */
public class Campanha {
	/**
	 * Id da Campanha
	 */
    private Long id;
    /**
     * Nome da Campanha
     */
    private String nome;
    /**
     * Descri��o da Campanha
     */
    private String descricao;
    /**
     * Mestre da Campanha
     */
    private Jogador mestre;
    /**
     * Data de cria��o da Campanha
     */
    private LocalDate dataCriacao;
    /**
     * Personagens da Campanha
     */
    private List<Personagem> personagens;

    /**
     * Getter de Id
     * @return Id da Campanha
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id da Campanha
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome da Campanha
     */
    public String getNome() {
        return nome;
    }
    /**
     * Setter de Nome
     * @param nome atribui o nome da Campanha
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Getter de Descri��o
     * @return Descri��o da Campanha
     */
    public String getDescricao() {
        return descricao;
    }
    /**
     * Setter de Descri��o
     * @param descricao atribui a Descri��o da Campanha
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    /**
     * Getter de Mestre
     * @return Mestre da Campanha
     */
    public Jogador getMestre() {
        return mestre;
    }
    /**
     * Setter de Mestre
     * @param mestre atribui o Mestre da Campanha
     */
    public void setMestre(Jogador mestre) {
        this.mestre = mestre;
    }
    /**
     * Getter da Data de Cria��o da Campanha
     * @return Data de Cria��o da Campanha
     */
    public LocalDate getDataCriacao() {
        return dataCriacao;
    }
    /**
     * Setter da Data de Cria��o da Campanha
     * @param dataCriacao atribui a Data de Cria��o da Campanha
     */
    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
    /**
     * Getter de Personagens da Campanha
     * @return Personagens da Campanha
     */
    public List<Personagem> getPersonagens() {
        return personagens;
    }
    /**
     * Setter de Personagens da Campanha
     * @param personagens atribui os Personagens da Campanha
     */
    public void setPersonagens(List<Personagem> personagens) {
        this.personagens = personagens;
    }
    /**
     * M�todo para adicionar Personagens a lista de 
     * Personagens da Campanha
     * @param personagem atribui um novo Personagem a 
     * Lista de Personagens da Campanha
     */
    public void addPersonagem(Personagem personagem) {
    	this.personagens.add(personagem);
    }
    
    /**
     * Construtor padr�o de Campanha
     */
    public Campanha() {
    	mestre = new Jogador();
    	personagens = new ArrayList<>();
    }
}
