/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

import java.util.*;

/**
 * Classe representativa do Jogador
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Jogador {
	/**
	 * Id do Jogador
	 */
    private Long id;
    /**
     * Nome do Jogador
     */
    private String nome;
    /**
     * Personagens do Jogador
     */
    private List<Personagem> personagens;

    /**
     * Getter de Id
     * @return Id do Jogador
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id do Jogador
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome do Jogador
     */
    public String getNome() {
        return nome;
    }
    /**
     * Setter de Nome
     * @param nome atribui o Nome do Jogador
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Getter de Personagens do Jogador
     * @return Personagens do Jogador
     */
    public List<Personagem> getPersonagens() {
        return personagens;
    }
    /**
     * Setter de Personagens
     * @param personagens atribui os Personagens do Jogador
     */
    public void setPersonagens(List<Personagem> personagens) {
        this.personagens = personagens;
    }
    
    /**
     * M�todo para adicionar novo Personagem a
     * Lista de Personagens de um Jogador
     * @param personagem adiciona um Personagem a
     * Lista de Personagens de um Jogador
     */
    public void addPersonagem(Personagem personagem) {
    	this.personagens.add(personagem);
    }

    /**
     * Construtor padr�o de Jogador
     */
    public Jogador() {
    
    }
}
